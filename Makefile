.PHONY: help test

all: help

build:
	python setup.py build --build-base=/tmp/build/dtrange

clean:
	find . \( -name "*~" -o -name "*.pyc" \) -delete

help:
	@echo make [ clean test sdist build upload install ]

install:
	python setup.py install

sdist:
	python setup.py sdist

test:
	for v in 2 3 ; do \
	python$$v -m unittest discover -v ; \
	done

upload:
	python setup.py sdist upload -r pypi
