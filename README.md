Overview
========
dtrange has multi-calendar datetime, date and time range iterators.

Features
--------
* Half-open or closed bounds.
* Non-uniform step date units, such as leap years and months.
* Alternative calendars: 360, all-leap, no-leap, gregorian, julian
* Data ordinals (total days) and time ordinals for all calendar types.
* Fraction arithmetic.
* [lower, upper) bounds search.
* Python 2 & 3.

Examples
========

Datetime
--------

    from datetime import datetime, timedelta
    from dtrange import dtrange

    start = datetime(2000, 1, 1)
    stop  = datetime(2020, 2, 1)
    step = timedelta(1)

    for it in dtrange(start, stop, step, endpoint=True):
        print(it)

    for units in ['y', 'm', 'w', 'd', 'h', 'min', 's', 'ms', 'us']:
        for it in dtrange(start, stop, step=1, units=units):
            print(units,it)

    for it in dtrange(start, stop, n=10):
        print(it)

    for it in dtrange(start, step, n=10):
        print(it)

    for units in ['y', 'm', 'w', 'd', 'h', 'min', 's', 'ms', 'us']:
        for it in dtrange(start, step=1, units=units, n=10):
            print(units,it)

    from dtrange import dtfraction
    
    print(dtfraction(datetime(2000,1,1), datetime(2001,1,1), datetime(2002,1,1)))
    
Date
----
    from datetime import date, timedelta
    from dtrange import drange

    start = date(2000, 1, 1)
    stop  = date(2005, 2, 1)
    step = timedelta(days=1)

    for calendar in ['360', 'gregorian', 'julian', 'leap', 'noleap']:
        for it in drange(start, stop, step, calendar=calendar):
            print(calendar,it)

    from dtrange import dfraction
    
    print(dfraction(date(2000,1,1), date(2001,1,1), date(2002,1,1)))

Time
----
    from datetime import time, timedelta
    from dtrange import trange

    start = time(0, 0, 0)
    stop  = time(0, 1, 0)
    step = timedelta(seconds=1)

    for it in trange(start, stop, step):
        print(it)

    from dtrange import tfraction
    
    print(tfraction(time(1,1,1), time(2,1,1), time(3,1,1)))

Testing
=======

    make test

Installation
============

From PyPi.

    pip install dtrange

From tarball.

    python setup.py install

License
=======

dtrange  Copyright (C) 2016  Remik Ziemlinski

This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under the
conditions of the GPLv3 license.
